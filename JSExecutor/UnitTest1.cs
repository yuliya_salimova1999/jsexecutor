using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace JSExecutor
{
    public class Tests
    {
        private IWebDriver _driver;
        private IJavaScriptExecutor _jsExecutor;

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _jsExecutor = (IJavaScriptExecutor)_driver;
        }

        [Test]
        public void Test1()
        {
            const string gitHubUrl = "https://github.com/";
            _driver.Navigate().GoToUrl(gitHubUrl);
            const string searchInputLocator = "//input[@aria-label='Search GitHub']";
            IWebElement webElement = _driver.FindElement(By.XPath(searchInputLocator));
            webElement.Click();
            _jsExecutor.ExecuteScript("arguments[0].innerHTML='WebDriver';", webElement);
            Assert.AreEqual("WebDriver", webElement.Text);
            _jsExecutor.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            _jsExecutor.ExecuteScript("window.scrollTo(0, -document.body.scrollHeight)");
        }

        [TearDown]
        public void Dispose()
        {
            _driver.Quit();
        }
    }
}